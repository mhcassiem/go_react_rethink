import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Message from './Message.jsx';

class MessageList extends Component {
    render() {
        return (
            <ul>
                {this.props.messages.map(message => {
                    return (
                        <Message key={message.createdAt} message={message} {...this.props} />
                    )
                })}
            </ul>
        );
    }
}

MessageList.propTypes = {
    messages: PropTypes.array.isRequired,
};

export default MessageList;