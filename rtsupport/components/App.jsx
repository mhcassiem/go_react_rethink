import React, { Component } from 'react';
import ChannelSection from "./channels/ChannelSection.jsx";
import UsersSection from "./users/UsersSection.jsx";
import MessageSection from "./messages/MessageSection.jsx";
import Socket from '../socket.js';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            connected: false,
            activeChannel: {},
            channels: [],
            users: [],
            messages: []
        };
    }

    componentDidMount() {

        let socket = this.socket = new Socket();
        socket.on('connect', () => this.onConnect());
        socket.on('disconnect', () => this.onDisconnect());
        socket.on('channel add', () => this.onAddChannel());
        socket.on('user add', () => this.onAddUser());
        socket.on('user edit ', () => this.onEditUser());
        socket.on('user remove', () => this.onRemoveUser());
        socket.on('message add', () => this.onMessageAdd());
    }

    onConnect() {
        this.setState({ connected: true });
        this.socket.emit('channel subscribe');
        this.socket.emit('user subscribe');
    }

    onDisconnect() {
        this.setState({ connected: false });
    }

    onAddChannel(channel) {
        let { channels } = this.state;
        channels.push(channel);
        this.setState({ channels });
    }

    addChannelFunction(name) {
        this.socket.emit('channel add', { name });
    }

    onAddUser(user) {
        let { users } = this.state;
        users.push(user);
        this.setState({ users });
    }

    onEditUser(editUser) {
        let { users } = this.state;
        users = users.map(user => {
            if (editUser.id === user.id) {
                return editUser;
            }
            return user;
        });
        this.setState({ users });
    }

    onRemoveUser(removeUser) {
        let { users } = this.state;
        users = users.filter(user => {
            return user.id !== removeUser.id;
        });
        this.setState({ users });
    }

    onMessageAdd(message) {
        let { messages } = this.state;
        messages.push(message);
        this.setState({ messages });
    }

    setChannelFunction(activeChannel) {
        this.setState({ activeChannel });
        this.socket.emit('message unsubscribe');
        this.setState({messages: []});
        this.socket.emit('message subscribe',
        {channelId: activeChannel.id});
    }

    addUserFunction(username) {
        this.socket.emit('add edit', { username });
    }

    setUserFunction(activeUser) {
        this.setState({ activeUser });
        // TODO call to get latest channel messages
    }

    addMessageFunction(content) {
        let { activeChannel } = this.state;
        this.socket.emit('add message',
            { channelId: activeChannel.id, content });
    }

    render() {
        return (
            <div className='app'>
                <div className='nav'>
                    <ChannelSection
                        {...this.state}
                        addChannel={(name) => this.addChannelFunction(name)}
                        setChannel={(activeChannel) => this.setChannelFunction(activeChannel)}
                    />
                    <UsersSection {...this.state}
                        setUser={(activeUser) => this.setUserFunction(activeUser)}
                        addUser={(username) => this.addUserFunction(username)}
                    />
                </div>
                <MessageSection
                    {...this.state}
                    addMessage={(content) => this.addMessageFunction(content)}
                />
            </div>
        );
    }
}

export default App;