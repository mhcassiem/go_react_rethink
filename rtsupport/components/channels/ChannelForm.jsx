import React, {Component} from 'react';
import PropTypes from 'prop-types';

class ChannelForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            channelName: ''
        }
    }

    onSubmitHandler(event) {
        event.preventDefault();
        const node = this.refs.channel;
        const channelName = node.value;
        this.props.addChannel(channelName);
        node.value = '';
    }

    render() {
        return (
            <form onSubmit={(event) => this.onSubmitHandler(event)}>
                <div className='form-group'>
                    <input className='form-control' placeholder="Add channel" type='text' ref='channel'/>
                </div>
            </form>
        )
    }
}

ChannelForm.propTypes = {
    addChannel: PropTypes.func.isRequired
};

export default ChannelForm;