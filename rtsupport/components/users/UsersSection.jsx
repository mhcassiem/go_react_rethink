import React, {Component} from 'react';
import PropTypes from 'prop-types';
import UserForm from "./UserForm.jsx";
import UserList from "./UserList.jsx";

class UsersSection extends Component {

    render() {
        return (
            <div className='support panel panel-primary'>
                <div className='panel-heading'>
                    <strong>Users</strong>
                </div>
                <div className='panel-body users'>
                    <UserList {...this.props} {...this.state} />
                    <UserForm {...this.props} />
                </div>
            </div>
        );
    }
}

UsersSection.propTypes = {
    users: PropTypes.array.isRequired,
    setUser: PropTypes.func.isRequired,
    addUser: PropTypes.func.isRequired
};

export default UsersSection;
