import React, {Component} from 'react';
import PropTypes from 'prop-types';

class User extends Component {

    onClickHandler(event) {
        event.preventDefault();
        const {setUser, user} = this.props;
        setUser(user);
    }

    render() {
        const {user} = this.props;
        return (
            <li>
                <a onClick={(event) => this.onClickHandler(event)}>
                    {user.username}
                </a>
            </li>
        );
    }
}

User.propTypes = {
    user: PropTypes.object.isRequired,
    setUser: PropTypes.func.isRequired
};

export default User;