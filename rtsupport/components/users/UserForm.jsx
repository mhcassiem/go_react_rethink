import React, {Component} from 'react';
import PropTypes from 'prop-types';

class UserForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userName: ''
        }
    }

    onSubmitHandler(event) {
        event.preventDefault();
        const node = this.refs.user;
        const userName = node.value;
        this.props.addUser(userName);
        node.value = '';
    }

    render() {
        return (
            <form onSubmit={(event) => this.onSubmitHandler(event)}>
                <div className='form-group'>
                    <input className='form-control' placeholder="Add user" type='text' ref='user'/>
                </div>
            </form>
        )
    }
}

UserForm.propTypes = {
    addUser: PropTypes.func.isRequired
};

export default UserForm;